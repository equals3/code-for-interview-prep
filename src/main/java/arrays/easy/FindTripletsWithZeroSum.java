//https://www.geeksforgeeks.org/find-triplets-array-whose-sum-equal-zero/

package arrays.easy;

import java.util.HashSet;
import java.util.Set;

public class FindTripletsWithZeroSum {
    public static void main(String[] args) {
        int arr[] = { 0, -1, 2, -3, 1 };
    //    findTripletsBruteForce(arr);
        findTripletsHashing(arr);
    }

    // O(n^2)
    private static void findTripletsHashing(int[] arr) {

        for(int i=0;i<arr.length-1;i++){

            Set<Integer> mySet = new HashSet<>();
            for(int j=i+1;j<arr.length;j++){

                int diff  = -(arr[i]+arr[j]);
                if(mySet.contains(diff)){
                    System.out.println(arr[i]+ " "+arr[j]+" "+diff);

                }
                else{
                    mySet.add(arr[j]);
                }

            }
            
        }
    }

    // O(n^3) compaxity
    private static void findTripletsBruteForce(int[] arr) {
        for (int i = 0; i < arr.length-2; i++) {
            for (int j = i+1; j < arr.length-1; j++) {
                for (int k = j+1; k <arr.length; k++) {
                    if (arr[i] + arr[j] + arr[k] == 0) {
                        System.out.println(arr[i] + " " + arr[j] + " " + arr[k]);

                    }
                }
            }
        }
    }
}
