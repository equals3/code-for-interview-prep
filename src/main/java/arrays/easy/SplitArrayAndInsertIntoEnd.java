//https://www.geeksforgeeks.org/split-array-add-first-part-end/

package arrays.easy;

import java.util.Arrays;

public class SplitArrayAndInsertIntoEnd {

    public static void main(String[] args) {
        int arr[] = { 12, 10, 5, 6, 52, 36 };
        int position = 2;

       splitArrayAndInsertIntoEnd(arr, arr.length, position);
        System.out.println(Arrays.toString(arr));

    }

    // O(nk) complexity
    private static void splitArrayAndInsertIntoEnd(int[] arr, int length, int position) {


        for(int i=0;i<position;i++){
            int temp =arr[0];
            for(int a=0;a<length-1;a++){
                arr[a]=arr[a+1];
            }
            arr[length-1]=temp;
        }
    }
}
