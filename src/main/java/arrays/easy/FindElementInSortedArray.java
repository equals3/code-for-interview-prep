//https://www.geeksforgeeks.org/find-element-sorted-array-whose-frequency-greater-equal-n2/

package arrays.easy;

public class FindElementInSortedArray {

    public static void main(String[] args) {
        int arr[] = { 1, 2, 2, 3 };
        // findMajority_Unsorted(arr);
        findMajority_sorted(arr);

    }

    private static int findMajority_sorted(int[] arr) {
        int len = arr.length;
        if (len % 2 == 0) { // if length is even
            if (arr[len / 2] == arr[len / 2 - 1]) {
                // both middle elements are same
                return arr[len / 2]; // or arr[len/2 - 1] // return whatever
            } else {
                if (arr[len / 2 - 1] == arr[0]) {
                    return arr[0];
                }

                else {
                    if (arr[len / 2] == arr[len - 1])
                        return arr[len - 1];
                    else
                        return 11;
                }
            }
        } else {
            // len is odd
            return arr[len / 2];
        }
    }

    private static void findMajority_Unsorted(int[] arr) {
        // if array is unsorted
        for (int i = 0; i < arr.length; i++) {
            int temp = arr[i];
            int count = 1;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] == temp) {
                    count++;
                }
            }
            if (count > (arr.length) / 2) {
                System.out.println(temp);
            }

        }
    }
}