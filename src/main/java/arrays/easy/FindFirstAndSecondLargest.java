// https://www.geeksforgeeks.org/find-second-largest-element-array/

package arrays.easy;
public class FindFirstAndSecondLargest{

    public static void main(String[] args) {
        int arr[]={2, 35, 1, 10, 34, 1};

        int first_max=0;
        int sec_max=0;

        for(int i=0;i<arr.length;i++){

            if(arr[i]>first_max){
                sec_max=first_max;
                first_max=arr[i];
            }
            else if (arr[i]>sec_max  && first_max != arr[i]){
                sec_max =arr[i];
            }
        }
        System.out.println(first_max);
        System.out.println(sec_max);

    }
}