package arrays.easy;

//https://www.geeksforgeeks.org/find-a-fixed-point-in-a-given-array/
public class FixedPointIntoArray {

    public static void main(String[] args) {
        int arr[] = { -10, -1, 0, 3, 10, 11, 30, 50, 100 };
        int result = linerSearchSolution(arr);
        System.out.println(result);
        int binarySearchResult = binarySearchSolution(arr);
        System.out.println(binarySearchResult);

    }

    private static int binarySearchSolution(int[] arr) {

        int left = 0;
        int right = arr.length - 1;
        int mid = -1;
        while (left <= right) {
            mid = (left + right) / 2;
            if(arr[mid]==mid){
                return mid;
            }
            else{
                if(arr[mid]>mid){
                    right =mid-1;
                }
                else{
                    left=mid+1;
                }
            }
        }
        return -1;
    }

    private static int linerSearchSolution(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == i) {
                return i;
            }
        }
        return -1;
    }
}