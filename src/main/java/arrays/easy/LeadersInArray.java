//https://www.geeksforgeeks.org/leaders-in-an-array/

package arrays.easy;

import java.util.Stack;

public class LeadersInArray {

    public static void main(String[] args) {
        int arr[] = new int[] { 16, 17, 4, 3, 5, 2 };
        // printLeadersBruteForce(arr);
        // printLeadersLinearTime(arr);
        printLeadersUsingStackSolution(arr);

    }

    private static void printLeadersUsingStackSolution(int[] arr) {

        Stack<Integer> myStack = new Stack<>();
        myStack.push(arr[arr.length-1]);
        for (int i = arr.length-1; i >=0; i--) {
            if (arr[i] > myStack.peek()) {
                myStack.push(arr[i]);
            }
          

        }

        while (!myStack.empty()) {
            System.out.println(myStack.pop());
        }
    }

    private static void printLeadersLinearTime(int[] arr) {

        int right = arr.length - 1;
        int max = 0;
        while (right >= 0) {

            if (arr[right] > max) {
                max = arr[right];
                System.out.println(max);
            }

            right--;

        }
    }

    // O(n*n)
    private static void printLeadersBruteForce(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int j = 0;
            for (j = i + 1; j < arr.length; j++) {
                if (arr[i] <= arr[j]) {
                    break;
                }
            }
            if (j == arr.length) {
                System.out.println(arr[i]);
            }
        }
    }
}