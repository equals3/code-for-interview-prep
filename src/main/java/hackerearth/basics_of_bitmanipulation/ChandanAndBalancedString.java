//https://www.hackerearth.com/practice/basic-programming/bit-manipulation/basics-of-bit-manipulation/practice-problems/algorithm/chandan-and-balanced-strings/

package hackerearth.basics_of_bitmanipulation;
public class ChandanAndBalancedString{

    public static void main(String[] args) {
        
        String str="abc";

        char arr[] = str.toCharArray();
        for(int i=0;i<arr.length;i++){
            for(int j=0;j<arr.length;j++){
                for(int k=i;k<=j;k++){
                    System.out.print(arr[k]+" ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
