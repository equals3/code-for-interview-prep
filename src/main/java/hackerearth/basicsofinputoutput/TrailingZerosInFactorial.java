//https://www.hackerearth.com/practice/basic-programming/implementation/basics-of-implementation/practice-problems/algorithm/trailing-zeroes-in-factorial/

package hackerearth.basicsofinputoutput;

public class TrailingZerosInFactorial{

    public static void main(String[] args) {
        System.out.println(findTrailingZeros(100000));
    }

     // Function to return trailing  
    // 0s in factorial of n 
    static int findTrailingZeros(int n) 
    { 
        // Initialize result 
        int count = 0; 
  
        // Keep dividing n by powers  
        // of 5 and update count 
        for (int i = 5; n / i >= 1; i *= 5) 
            count += n / i; 
  
        return count; 
    } 
}