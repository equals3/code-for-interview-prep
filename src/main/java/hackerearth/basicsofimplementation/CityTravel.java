//https://www.hackerearth.com/practice/basic-programming/implementation/basics-of-implementation/practice-problems/algorithm/city-travel-59bad87f/

package hackerearth.basicsofimplementation;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class CityTravel {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input[] = br.readLine().split("\\s+");
        long S = Integer.parseInt(input[0]);
        long X = Integer.parseInt(input[1]);
        int N = Integer.parseInt(input[2]);

        Map<Long, Long> inputMap = new TreeMap<>();

        for (int i = 0; i < N; i++) {
            String exceptions[] = br.readLine().split("\\s+");
            inputMap.put(Long.parseLong(exceptions[0]), Long.parseLong(exceptions[1]));
        }
        Long numDays = new Long(0);
        Long ONE = new Long(1);
        Iterator<Map.Entry<Long, Long>> it = inputMap.entrySet().iterator();
        while (it.hasNext() && S > 0) {
            Map.Entry<Long, Long> me = it.next();
            Long t = me.getKey();    // 2  4 
            Long y = me.getValue(); // 4  8
            Long diff = t - numDays - ONE; //2-0-1=1   , 4-2-1 =1
            Long sumDiff = diff * X; // 1*5=5 , sumdif=5;
            if (sumDiff >= S)
                break;
            else {
                S = S - sumDiff;   //21-5;  12-5;
                S = S - y;         //16-4;  7-8; -1
                numDays = t;       //  2  , numdays=4;
            }
        }
        if (S > 0)
        {
            numDays += S / X;
            if (S % X > 0)
                numDays += ONE;
        }
        
        System.out.println(numDays);

    }
}