//https://www.geeksforgeeks.org/run-length-encoding/

package classic_interview_questions.string;

import java.util.Arrays;

public class RunLengthEncoding{

    public static void main(String[] args) {
        String input = "aaebbfbbcccddddeefffa";
        runLengthEncoding_Sorting(input);
    }

    private static void runLengthEncoding_Sorting(String input) {
        char [] ch = input.toCharArray();
         Arrays.sort(ch);
        int count=1;
        StringBuilder sb = new StringBuilder();
         for(int i=0;i<ch.length;i++){

            if((i<ch.length-1) && ch[i]==ch[i+1]){
                count++;
            }
            else{
                sb.append(ch[i]);
                sb.append(count);
                count=1;
            }
         }
         System.out.println(sb.toString());

    }
}