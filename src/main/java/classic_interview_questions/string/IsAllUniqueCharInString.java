//geeksforgeeks.org/determine-string-unique-characters/

package classic_interview_questions.string;

import java.util.Arrays;

public class IsAllUniqueCharInString{

    public static void main(String[] args) {
        String input = "GeeksforGeeks"; 
       // System.out.println(isAllUniqueCharInString_BruteForce(input));
        System.out.println(isAllUniqueCharInString_SortingSolution(input));
    }

    // sorting soln O(nlogn)
    private static boolean isAllUniqueCharInString_SortingSolution(String input) {
        
        char[] charArr =  input.toCharArray();
        Arrays.sort(charArr);
        for(int i=0;i<charArr.length-1;i++){
            
            if(charArr[i]==charArr[i+1]){
                return false;
            }
        }
        return true;

    }

    // brute force O(n^2) duh !
	private static boolean isAllUniqueCharInString_BruteForce(String input) {

        for(int i=0;i<input.length();i++){
            for(int j=i+1;j<input.length();j++){
                if(input.charAt(i)==input.charAt(j)){
                    return false;
                }
            }
        }
        return true;

	}
}