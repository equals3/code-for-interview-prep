//https://www.geeksforgeeks.org/least-frequent-element-array/

package classic_interview_questions.arrays;

import java.util.Arrays;

public class LeastFrequentElementInArray{

    public static void main(String[] args) {
        
        int arr[] = {1, 3, 2, 1, 2, 2, 3, 1}; 
        leastFrequentElementInArray_SortingSolution(arr);
    }

    private static void leastFrequentElementInArray_SortingSolution(int[] arr) {

        Arrays.sort(arr);
         int curr_count=1;
         int result=-1;
         int minimum_so_far= Integer.MAX_VALUE;
        for(int i=0;i<arr.length-1;i++){
            
            if(arr[i]==arr[i+1]){
                curr_count++;
            }
            else{

                if(curr_count<minimum_so_far){
                    minimum_so_far=curr_count;
                    result =arr[i];
                }
            curr_count=1;
            }
        }
        if (curr_count < minimum_so_far) 
        { 
            minimum_so_far = curr_count; 
            result = arr[arr.length - 1]; 
        } 
        System.out.println(result);
    }
}