
package classic_interview_questions.arrays;

import java.util.Arrays;
// How'd maintain order
public class SegregateOddEvenNumbers{

    public static void main(String[] args) {
         int arr[] = {1,2,3,4,5,6,7,8,9,10};
         int left =0;
         int right = arr.length-1;

         while(left<=right){

            while(arr[left]%2==0){
                left++;
            }
            while(arr[right]%2!=0){
                right--;
            }
            if(left<right){
                //swap
                int temp =  arr[left];
                arr[left]=arr[right];
                arr[right]= temp;
            }
         }
         System.out.println(Arrays.toString(arr));
    }
}
