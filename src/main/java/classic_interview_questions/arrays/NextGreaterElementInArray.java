// https://www.geeksforgeeks.org/next-greater-element/

package classic_interview_questions.arrays;

public class NextGreaterElementInArray{


    static class CustomStack{

        int top=-1;
        int size =0;
        int items[];
        
        public  CustomStack(int size){
            this.items=new  int[size];
        }
        public void push(int item){
            items[++top]=item;
        }
        public int pop(){
            return items[top--];
        }
        public int peek(){
            return items[top];
        }
        public boolean isEmpty(){
            return top==-1;
        }

    }

    public static void main(String[] args) {

        int arr[]= {11, 13, 21, 3};
         
      //  nextGreaterElementInArray_BruteForce(arr);
        nextGreaterElementInArray_StackSolution(arr);

        
   }

   // O(n) complexity

    private static void nextGreaterElementInArray_StackSolution(int[] arr) {

        CustomStack stack =  new CustomStack(100);
     //   stack.top=-1;

        // insert fist element into stack to compare

        stack.push(arr[0]);
        // 11 13 21 3
        for(int i=1;i<arr.length;i++){
        if(arr[i]>stack.peek()){
            stack.push(arr[i]);
            break;
        }
        }

        while(!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }

    // O(n^2) complexity
    private static void nextGreaterElementInArray_BruteForce(int[] arr) {

        for(int i=0;i<arr.length;i++){
            int next_greater =-1;
            for(int j=i+1;j<arr.length;j++){
                if(arr[i]<arr[j]){
                    next_greater=arr[j];
                    break;
                }
            }
            System.out.println(next_greater);

        }

    }

}