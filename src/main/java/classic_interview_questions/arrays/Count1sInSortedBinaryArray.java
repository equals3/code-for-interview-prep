// https://www.geeksforgeeks.org/count-1s-sorted-binary-array/

package classic_interview_questions.arrays;

public class Count1sInSortedBinaryArray{

    public static void main(String[] args) {
        int arr[] = {1, 1, 1, 0, 0, 0, 0};
     //  int res1= count1s_LinearSolution(arr);
      // System.out.println(res1);
       int res2= count1s_TwoPointerSolution(arr);
       System.out.println(res2);

    }

    private static int count1s_TwoPointerSolution(int[] arr) {

        int left =0;
        int right = arr.length-1;
        int count=0;
        while(left<=right){
            if(arr[left]==1){
            count++;
            }
            left++;
            right--;
        }
        return count;
    }

    private static int count1s_LinearSolution(int[] arr) {
        int count =0;
        for(int i=0;i<arr.length;i++){
            if(arr[i]==1){
                count++;
            }
            else{
                break;
            }
        }
        return count;
    }
}