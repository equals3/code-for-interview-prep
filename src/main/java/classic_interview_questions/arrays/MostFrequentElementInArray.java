//https://www.geeksforgeeks.org/frequent-element-array/

package classic_interview_questions.arrays;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MostFrequentElementInArray {
    public static void main(final String[] args) {
        final int arr[] = { 10, 20, 10, 20, 30, 20, 20 };
       // mostFrequentElementInArraySortingSolution(arr);
       mostFrequentElementInArrayHashingSolution(arr);
    }

    //O(n) soln
    private static void mostFrequentElementInArrayHashingSolution(final int[] arr) {
   

        Map<Integer,Integer> map =  new HashMap<>();

        for(int i=0;i<arr.length;i++){
            
            Integer val = map.get(arr[i]);
            if(val==null){
                map.put(arr[i], 1);
            }
            else{
                map.put(arr[i], val+1);

            }
        }
        int  max= 1;
        int result =-1;

        for(Map.Entry<Integer,Integer>innerMap:map.entrySet()){

            if(innerMap.getValue()>max){
                result=innerMap.getKey();
                max=innerMap.getValue();
            }
        }
        System.out.println(result);
        System.out.println(max);
    
    }

    private static void mostFrequentElementInArraySortingSolution(final int[] arr) {

        Arrays.sort(arr);
        int max_count = 1, res = arr[0]; 
        int curr_count = 1; 
        for(int i=0;i<arr.length-1;i++){
            if(arr[i]==arr[i+1]){
             curr_count++;
            }
            else{
                if(curr_count>max_count){
                    max_count=curr_count;
                    res= arr[i];
                }
                //
                curr_count=1;
            }
        }
        if (curr_count > max_count) 
        { 
            max_count = curr_count; 
            res = arr[arr.length-1]; 
        } 
        System.out.println(res);
    }
}